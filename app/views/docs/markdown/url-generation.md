## URL Generation
[Introduction](#introduction)  
[Class Alias](#class-alias)  
[URL Redirection](#url-redirection)

<a name="introduction"></a>
### Introduction
URL generation is built into Scara to make it easy to create both relative and absolute URLs. While the generator is mostly for relative URLs, absolute ones can be generated as well.

### Generation
There are three methods for generating URLs: `URL::to()`, `URL::generateUrl()`, and `URL::asset()`

#### `URL::to()` {.header-method .first-method}
`to` is used for quickly getting a URL that you can use for going to another web page or site

```php
$url = $generator->to('/about');

// http://localhost/scara/about
```

#### `URL::generateUrl()` {.header-method}
Conversely, you can use `generateUrl` to generate a URL. `generateUrl` is the base for the other URL generation methods.

```php
$url = $generator->generateUrl('/about');

// http://localhost/scara/about
```

#### `URL::asset()` {.header-method}
`asset` is used to generate a URL specifically for loading in an asset from the app's assets directory

```php
$bootstrap = $generator->asset('css/bootstrap.min.css');

// http://localhost/scara/assets/css/bootstrap.min.css
```

<a name="class-alias"></a>
### Class Alias
While you can initialize the UrlGenerator class, you can also use the URL facade

```php
$asset = URL::asset('css/bootstrap.min.css');

// http://localhost/scara/assets/css/bootstrap.min.css

$google = URL::to('http://google.com');

// http://google.com
```

<a name="url-redirection"></a>
### URL Redirection
While the UrlGenerator is a generation tool, a convenience method for URL redirection is also in the generatior. `URL::redirect()` will take a given URL, generate an absolute one assuming it's a relative URL, and redirect to the given URL

```php
URL::redirect('/about');
```
