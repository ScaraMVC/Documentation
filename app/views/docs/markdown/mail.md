## Mail
[Introduction](#introduction)  
[Configuration](#configuration)  
[Building an Email](#building-an-email)  
[Adding Files/Recipients](#adding-files-recipients)  
└[Files](#files)  
└[Recipients](#recipients)  
  └[addAddress](#add-address)  
  └[cc](#cc)  
  └[bcc](#bcc)  
[Sending Your Email](#sending-your-email)

<a name="introduction"></a>
### Introduction
Scara has a built in mail driver which uses PHPMailer to send emails. While you can certainly just use PHPMailer since Scara already includes it, the mail driver makes it easier to quickly draft up emails and send them. Scara even has some configuration to easily set up the mail driver.

<a name="configuration"></a>
### Configuration
Scara's mail configuration can be found in `config/mail.php` along with instructions on each configuration option. While a driver is selectable, for now, Scara only supports PHPMailer.

SMTP options are available if you wish to enable/disable SMTP along with options to set up SMTP settings and adding the user who will send the emails. Unless of course, you select a different email for the Mail's `from` option when constructing your email

<a name="building-an-email"></a>
### Building an Email
To build an email in Scara, use the Mail driver, and create the email's structure

```php
$mail = Mail::create([
    'to' => 'john.doe@example.com',
    'subject' => 'Test Email',
]);
```

You can also add a from option in the creation if you want to use a different email from what's supplied in your mail configuration.

You should also add a body in one of two ways

```php
// adding body via property
$mail->body = "My Email Body";

// adding body via method
$mail->addBody("My Email Body");
```

If you want your body to be an HTML email, using the HTML property will set this for you. By default, it's set to true. If you want to disable it, set it to false

```php
// disable html in email
$mail->html = false;
```

<a name="adding-files-recipients"></a>
### Adding Files/Recipients
<a name="files"></a>
#### Files
PHPMailer supports attachments, therefore the mail driver also supports this. To add an attachment to your email, simply use the `attach` method

```php
$mail->attach("/path/to/my/file");
```

<a name="recipients"></a>
#### Recipients
By default, when constructing your email, you give a `to` option. If you with to add more, there are a few options for you

<a name="add-address"></a>
##### `addAddress()` {.header-method}
Using `addAddress` will add another address to the recipient list

```php
$mail->addAddress('another.email@domain.com');
```

<a name="cc"></a>
##### `cc()` {.header-method}
The `cc` method adds a CC to your email

```php
$mail->cc('another.email@domain.com');
```

<a name="bcc"></a>
##### `bcc()` {.header-method}
The `bcc` method adds a BCC to your email

```php
$mail->bcc('another.email@domain.com');
```

<a name="sending-your-email"></a>
### Sending Your Email
To send your email, use the `send()` method. This method returns a boolean, so you can use it in an `if` statement to do a check of whether or not your email sent successfully

```php

if ($mail->send()) {
    echo "Email sent!";
} else {
    echo "Email not sent!";
}
```
