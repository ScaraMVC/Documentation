## Hashing
[Introduction](#introduction)  
[Hashing](#hashing) 

<a name="introduction"></a>
## Introduction
Scara has hashing built in, utilizing PHP's `password_hash` and `password_verify` functions. There are two methods you can call for creating/verifying hashes

<a name="hashing"></a>
## Hashing
### Creating Hashes
To create a new hash, call the make function

```php
$hash = Hash::make('string to hash');

// $2y$10$ovoOo9wvd8DpfHyDL06V6.oGNhiKKxKBgZaul8e55Ond1ws4RXiRu
```

You can also add in the cost to be used for calculating the new hash

```php
$hash = Hash::make('string to hash', 10);

// $2y$10$ovoOo9wvd8DpfHyDL06V6.oGNhiKKxKBgZaul8e55Ond1ws4RXiRu
```

### Verifying Hashes
Verifying hashes are as easy as creating them

```php

$hash = "$2y$10$ovoOo9wvd8DpfHyDL06V6.oGNhiKKxKBgZaul8e55Ond1ws4RXiRu";

$check = (Hash::check('string to hash', $hash)) ? "verified" : "invalid";

// verified/invalid
```

### Rehashing
If you need to rehash a string because the cost was changed between the original hash and a new one, try the following

```php
$newhash = "$2y$10$RY/jPbU418tz70eW4zs1fuC5FvUb6NKXusR9CALyLgjAx/qdr.Udy";

if(Hash::needsRehashing($newhash))
{
	$hash = Hash::make('string to hash');
}
```
