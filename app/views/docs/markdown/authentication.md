## Authentication
[Introduction](#introduction)  
[Checking for Authentication](#checking-for-authentication)  
[Authentication](#authentication)  
└[Configuration](#configuration)  
└[Attempting User Login](#attempting-user-login)  
└[Logging the User Out](#logging-the-user-out)  
[Getting User Information](#getting-user-information)

<a name="introduction"></a>
### Introduction
Scara has built in authentication, and can be used to authenticate a user. Scara's Auth class has the ability to login/logout/check authentication, and even quickly grab user's information

<a name="checking-for-authentication"></a>
### Checking for Authentication
To check if a user is logged in, simply calling `check()` is sufficient enough

```php
...
if (Auth::check()) {
    echo "User is logged in!";
} else {
    echo "User is not logged in!";
}
...
```

<a name="authentication"></a>
### Authentication

<a name="configuration"></a>
#### Configuration
Before you can log in a user, you need to configure Scara's authentication configuration in `config/app.php`

```php
...
// The table that will be used for user authentication
// Don't include prefix! This will be automatically detected
'auth_table'        => 'users',
// What driver should the session run?
// Session, Cookie, or File (File not supported yet)
'session'           => 'file',

// If you chose file sessions, where are they stored?
'session_location'  => app_path().'/../storage/sessions/',

// Session decrypt token
// Must be 16, 24, or 32 characters
'token'             => 'YourAppSessionToken',
...
```

`auth_table` is used to determine what table in your database to authenticate against  
`session` sets the session driver. Depending on the kind of session you want to use  
`session_location` is used to find where to store sessions if you are using the file driver  
`token` is the session decryption token. Sessions are encrypted with OpenSSL using this token


<a name="attempting-user-login"></a>
#### Attempting User Login
To attempt a login of the user, you need at lease one item to authenticate with. Username/Password is usually the norm, but Scara will let you get away with any combination of items you wish

```php
$auth = Auth::attempt([
    'username' => 'some_username',
    'password' => Hash::make('some_password'),
]);

if ($auth) {
    "User authenticated and logged in!";
}
```

<a name="logging-the-user-out"></a>
#### Logging the User Out
Logging out a user is as easy as checking if a user is authenticated

```php
Auth::logout();
```

Logging a user out will remove any session items generated during the authentication process

<a name="getting-user-information"></a>
### Getting User Information
If you authenticate a user by the users database table, or by one of your choosing that holds some user data, calling the `user()` method will return the data for that user

```php
$user = Auth::user();

echo $user->username;

// some_username
```
