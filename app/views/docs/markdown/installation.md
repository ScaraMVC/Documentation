## Installation
[System Requirements](#system-requirements)  
[Setting Up](#setting-up)  
[Configuration](#configuration)  
[Dummy Server](#dummy-server)

<a name="system-requirements"></a>
### System Requirements
Scara uses libraries which utilize some of the latest features in PHP, so you'll notice older versions of PHP aren't supported, sorry..

Here is a list of the required prerequisites for your server

* PHP >= 5.6
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension

<a name="setting-up"></a>
### Setting Up
Scara uses [Composer](https://getcomposer.org/) for everything, so you'll need it.

To create a new Scara project, execute the following:

```bash
$ composer create-project scaramvc/scara myproject --prefer-dist
```

<a name="configuration"></a>
### Configuration
Next, configure Scara to meet the requirements of your server. Currently, there are only two configuration files by default: `config/app.php` and `config/database.php`

In the app config, change the following to fit your server:

```php
'appname' => 'scara' // Application's name
'basepath' => '/scara' // Base path of the application
```

<a name="dummy-server"></a>
### Dummy Server
Scara comes with a dummy server script which can be used to run a PHP server via the command line. To do so, in the project's root directory, run

```shell
$ php -S 127.0.0.1:8080 -t . server.php
```
