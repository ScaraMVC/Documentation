## Configuration
[Introduction](#introduction)  
[Accessing Configurations](#accessing-configurations)  
[Config Alias](#config-alias)  
[Config Helpers](#config-helpers)

<a name="introduction"></a>
### Introduction
Scara has a built in Configuration class, which can be accessed either through the normal class instantiation, by using Scara's Config facade, or one of the helper functions. All configuration is stored in the `config` directory under the application's root directory.

<a name="accessing-configurations"></a>
### Accessing Configurations
In order to access configuration values, you need to include the configuration class, and from there you can load in a configuration. Configuration files are basically just PHP scripts that return an array, and the Configuration class loads these files for you. The class uses a "lazy-load" system, basically it won't load a configuration unless you ask for it.

To load in a configuration script, try the following

```php
...
use Scara\Config\Configuration;

$cc = new Configuration; // Initialize class
$config = $cc->from('app'); // Load app config script
$appname = $config->get('appname') // Get app name value from appname key
...
```

<a name="config-alias"></a>
### Config Alias
While using the above method is perfectly fine, assuming you want to load in a configuration value without having to initialize a new Configuration instance, you can always utilize the Config alias.

```php
$appname = Config::get('appname', 'app');
```

This will open `config/app.php` and look for the key `appname`. You can also seperate keys by using a period in between keys:

```php
$configAlias = Config::get('aliases.Config', 'app');
```

The option of loading a configuration file without actually getting a value is also open to you. Using Config's `from()` method will load in the given configuration file

```php
Config::from('app');
```

This method returns an instance of the Config class, which opens up the option of chaining the `from()` and `get()` methods

```php
$appname = Config::from('app')->get('appname');
```

<a name="config-helpers"></a>
### Config Helpers
In addition to regular configuration loading and the Config facade, you can also use the `config_` helper functions, which act just like the Config facade.

```php
// load in file
$config = config_from('app');

// get value
$appname = $config->get('appname');

// from/get chaining
$appname = config_from('app')->get('appname');

// loading file and getting value
$appname = config_get('appname', 'app');
```

The difference in `Config::get()` and the `config_get()` helper is that the second parameter is required for the helper function. Since you're not storing a configuration variable and loading with the helper function, you are required to specify which configuration file you need to load the value from
