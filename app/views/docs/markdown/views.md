## Views
[Introduction](#introduction)  
[Rendering Views](#rendering-views)  
[Passing Data to Views](#passing-data-to-views)

<a name="introduction"></a>
### Introduction
Views are the final piece of the Scara workflow. Views are the layouts that the user sees in the end. Views in Scara are powered by Laravel's Blade Templating Engine. If you wish to learn about Blade, please <a href="https://laravel.com/docs/5.3/blade" target="_blank">visit this page</a>. This documentation on Views are specific to Scara only, and will not cover Blade.

<a name="rendering-views"></a>
### Rendering Views
Since views are based from Blade, using any default Blade command within your view is perfectly okay. All views are user `app/views` and all have the extension `.blade.php`. To render a view from your controller, run the controller's `render()` method with the supplied view path and name

```php
$this->render('home.index');
```

<a name="passing-data-to-views"></a>
### Passing Data to Views
Passing data over to views is pretty simple, however there is a caveat, data **MUST** be passed to a view **BEFORE** rendering it

```php
$this->with('mydata', $mydata)->render('home.index');
```

Same goes with flash messages

```php
$this->flash('message', 'This is a flash message')->render('home.index');
```

The reason for this is due to how Scara's view class handles rendering views. Rendering a view is the last thing View will do, and processes all information before making it's final round.

While you can chain your commands, controllers also have a `renderWithData()` method you can use instead if you so choose

```php
$this->renderWithData('home.index', ['data' => $mydata]);
```
