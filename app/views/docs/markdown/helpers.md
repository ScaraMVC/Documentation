## Helpers
[Introduction](#introduction)  
[Available Helpers](#available-helpers)  
[Helper Function Demonstration](#helper-function-demonstration)

<a name="introduction"></a>
### Introduction
Scara has a few helper functions built in. Since Laravel's Support module is included within Scara's framework, natually all of the [helper functions](https://github.com/laravel/framework/blob/5.3/src/Illuminate/Support/helpers.php) provided by the module are available for use.

<a name="available-helpers"></a>
### Available Helpers
Below is a list of the available helper methods provided by Scara. The ones provided by Laravel's Support module are **NOT** included in this list

#### Basic Functions [<span class="caret"></span>](#basic-functions)
[app_path](#app-path)  
[base_path](#base-path)  
[storage_path](#storage-path)  
[printr](#printr)  
[vd](#vd)  
[version](#version)

#### Configuration Functions [<span class="caret"></span>](#configuration-functions)
[config_from](#config-from)  
[config_get](#config-get)  
[config_path](#config-path)

#### HTML Functions [<span class="caret"></span>](#html-functions)
[asset](#asset)  
[link_to](#link-to)  
[url](#url)

#### Session Functions [<span class="caret"></span>](#session-functions)
[session_delete](#session-delete)  
[session_flash](#session-flash)  
[session_get](#session-get)  
[session_has](#session-has)  
[session_set](#session-set)

<a name="helper-function-demonstration"></a>
### Helper Function Demonstration

<a name="basic-functions"></a>
#### Basic Functions {.header-heading}
<a name="app-path"></a>
#### `app_path()` {.header-method .first-method}
The `app_path` function gets Scara's app path, which is defined by the app's base path plus the app directory

```php
$appPath = app_path();

// /var/www/scara/app
```

<a name="base-path"></a>
#### `base_path()` {.header-method}
The `base_path` function gets the base path of the application. Not to be confused with `Config::get('basepath', 'app')`

```php
$basepath = base_path();

// /var/www/scara
```

<a name="storage-path"></a>
#### `storage_path()` {.header-method}
The `storage_path` function gets the path of Scara's storage directory

```php
$storagepath = storage_path();

// /var/www/scara/storage
```

<a name="printr"></a>
#### `printr()` {.header-method}
The `printr` function uses PHP's `print_r` function and returns it surrounded in `<pre>` tags

```php
$array = [
    'name' => 'John',
    'age' => 27,
];

printr($array);

// Array
// (
//    [name] => John
//    [age] => 27
// )
// 1
```

<a name="vd"></a>
#### `vd()` {.header-method}
The `vd` function uses PHP's `var_dump` function and returns it surrounded in `<pre>` tags

```php
$array = [
    'name' => 'John',
    'age' => 27,
];

vd($array);

// array(2) {
//  ["name"]=>
//  string(4) "John"
//  ["age"]=>
//  int(27)
// }
```

<a name="version"></a>
#### `version()` {.header-method}
The `version` function returns Scara's current version directly from Packagist

```php
$version = version();

// 1.0-rc2
```

<a name="configuration-functions"></a>
#### Configuration Functions {.header-heading}
<a name="config-path"></a>
#### `config_path()` {.header-method .first-method}
The `config_path` function gets the configuration's path

```php
$configPath = config_path();

// /scara/config
```

<a name="config-from"></a>
#### `config_from()` {.header-method}
The `config_from` function works like `Config::from()` in that it loads in the requested configuration file.

Either create a variable

```php
$config = config_from('app');
```

or chain it with Configuration's `get` method

```php
$appname = config_from('app')->get('appname');

// My Scara Website
```

<a name="config-get"></a>
#### `config_get()` {.header-method}
The `config_get` function gets a configuration value from a specified configuration file. Unlike `Config::get` the second parameter for the configuration file is **required**

```php
$appname = config_get('appname', 'app');

// My Scara Website
```

<a name="html-functions"></a>
### HTML Functions {.header-heading}
<a name="asset"></a>
#### `asset()` {.header-method .first-method}
The `asset` function uses the `Html::asset` method to generate an asset URL for files within the assets directory

```php
$url = asset('css/bootstrap.min.css');

// http://localhost/scara/assets/css/bootstrap.min.css
```

<a name="link-to"></a>
#### `link_to()` {.header-method}
The `link_to` function uses the `Html::link` method to generate an HTML anchor tag

```php
$a = link_to('http://google.com', 'Google', ['target' => '_blank']);

// <a href="http://google.com" target="_blank">Google</a>
```

<a name="url"></a>
#### `url()` {.header-method}
The `url` function uses the `Html::url` method to generate a url

```php
// relative url
$url = url('/test');

// http://localhost/scara/test

$url = url('http://google.com');

// http://google.com
```

<a name="session-functions"></a>
#### Session Functions {.header-heading}
<a name="session-delete"></a>
#### `session_delete()` {.header-method .first-method}
The `session_delete` function removes a session using the given key

```php
session_delete('name');
```

<a name="session-flash"></a>
#### `session_flash()` {.header-method}
The `session_flash` function does two things: sets a flash message, or retrieves one

```php
// set flash message
session_flash('msg', 'This is a flash message');

// get flash message
$msg = session_flash('msg');

// This is a flash message
```

<a name="session-get"></a>
#### `session_get()` {.header-method}
The `session_get` function retrieves a session value using the given key

```php
$name = session_get('name');

// John
```

<a name="session-has"></a>
#### `session_has()` {.header-method}
The `session_has` function checks if a given key exists in the session, and returns a boolean depending on the outcome

```php
$hasName = session_has('name');

// true

$hasAge = session_has('age');

// false
```

<a name="session-set"></a>
#### `session_set()` {.header-method}
The `session_set` method sets a new value into the session with the given key

```php
session_set('age', 27);
```
