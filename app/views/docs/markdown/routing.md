## Routing
[Introduction](#introduction)  
[The Router](#the-router)  
[URL Routes](#url-routes)  
[Route Actions](#route-actions)

<a name="introduction"></a>
### Introduction
Scara handles URL schemas using it's built in Router system. All routes defined for your Scara application will be under `app/routes.php`

<a name="the-router"></a>
### The Router
To create a new route, use the `$router` object inside `app/routes.php` to define a **GET** or **POST** HTTP request.

```php
// get request
$router->get('/', 'HomeController@getIndex');

// post request
$router->post('/', 'HomeController@postIndex');
```

Using the `$router` object gives you the ability to create a GET or POST request. The `get()` and `post()` methods take 2 parameters: the URL, and the action.

<a name="url-routes"></a>
### URL Routes
URLs in the router can be static routes such as 

```php
// static route
$router->get('/users/login', 'UsersController@getLogin');
```

or by using variables

```php
// variable route
$router->get('/users/{username}', 'UsersController@getUser');
```

Using variables in routes gives you access to the Request object within the controller being called upon by the route.

<a name="route-actions"></a>
### Route Actions
Actions are defined in two ways, calling the controller@method, or by using an array

```php
// controller@method
$router->get('/', 'HomeController@getIndex');

// array
$router->get('/', [
    'uses' => 'HomeController@getIndex'
]);
```

An action is defined by two parts, the controller, and the method. There are no required ways of naming either a controller or method, and can be named anything.
