## Models
[Introduction](#introduction)  
[Making Models](#making-models)  
[Getting Model Data](#getting-model-data)  
[Updating Model Data](#updating-model-data)  
[Deleting Model Data](#deleting-model-data)  
[Creating/Running Migrations](#creating-running-migrations)  
[Deleting Migrations](#deleting-migrations)

<a name="introduction"></a>
### Introduction
Models in Scara are simple to work with. Models handle the grabbing of data from a database, which can be used for any sort of data handling

<a name="making-models"></a>
### Making Models
To make a new model, go to `app/models` and create a new model.

A model's code should resemble this

```php
<?php

namespace App\Models;

use Scara\Http\Model;

class User extends Model
{
    protected $table 'users';

    protected $fillable = ['username', 'password'];
}
```

This tells Scara's base model to set the table and mutable items for the model. If an item is not specified in the `$fillable` variable, it will not be writable. Attempting to write to an item not specified here will result in nothing happening, and no value being passed into the database

Defining models are easy, since the child models of the parent `Model` class are simple, and serve to define the model. All other functions are handled by the base model class.

<a name="getting-model-data"></a>
### Getting Model Data
To get model data, you need to run the `init()` method to initialize the use of that model, and call the `where()` method to select an item

```php
$user = User::init()->where('username', '=', 'some_username')->first();
```

`where()` let's you select an item based on the key and value, and using one of the checking operations. e.g. `=` `<` `>` etc. `first()` will grab the first item it can. Keep in mind first returns an `stdObject` so you cannot use the variable that first is called on. If you make changes, you'll have to `init()` your model again

You do not have to call first, you can either loop through results, or do a head count. `count()` will let you check the number of results returned

```php
$users = User::init()->where('username', '=', 'some_username');

$count = $users->count();

// 1
```

If you just want to get an item by it's id, using `find()` will do just that

```php
$user = User::init()->find(1);
```

<a name="updating-model-data"></a>
### Updating Model Data
You can update data for your model, but in order to save changes, you need to reinitialize your model, and call it's save method with your changed data

```php
$user = User::init()->find(1)->first();
$user->email = 'new.email@domain.com';

if (User::init()->save($user)) {
    ...
}
```

<a name="deleting-model-data"></a>
### Deleting Model Data
You can delete a record from the database with your model by calling it's `delete()` method, and supplying the record's ID

```php
User::init()->delete(1);
```

<a name="creating-running-migrations"></a>
### Creating/Running Migrations
The best thing about database administration is the ability to create simple and easy to read scripts that will execute and push/pull tables to the database. For Scara, this system of administration is in the form of migrations.

To create a new migration, use Scara's CLI interface

```shell
$ php cli db:make create_users_table
```

This generates a new migration in `database/migrations/YYYY_MM_DD_HHMMSS_create_users_table.php` which looks like

```php
<?php

use Scara\Database\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        // run when pushing to database
    }

    pubilc function down()
    {
        // run when rolling back changes
    }
}
```

To add new data, you need to call `Migration`'s `create()` method, supplying the table name and a closure

```php
public function up()
{
    $this->create('users', function($table)
    {
        $table->increments('id');
        $table->string('username');
        $table->string('password');
        $table->timestamps();
    });
}
```

To drop the table, use the `drop()` method in the `down()` method of your migration

```php
public function down()
{
    $this->drop('users');
}
```

To migrate changes, run

```shell
$ php cli db:push
```

To rollback changes

```shell
$ php cli db:rollback
```

Both `db:push` and `db:rollback` take an optional argument for the name of a migration, assuming you want to push/pull a single migration at a time

<a name="deleting-migrations"></a>
### Deleting Migrations
<div class="alert alert-danger" role="alert">  
<b>WARNING:</b> Running this command is irreversable and potentially dangerous. Execute this command at your own risk
</div>
While you might create a migration my accident, it's easy to delete them as well, just run

```php
$ php cli db:delete create_users_table
```

This call will run the migration's `down()` method removing it from the database if it's there, and remove it from your file system and cache.
