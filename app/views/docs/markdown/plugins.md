## Plugins
[Introduction](#introduction)  
[Service Providers](#service-providers)  
[Facades](#facades)  
[Configuration](#configuration)  
[Plugins in Action](#plugins-in-action)  
└ [The Source Code](#the-source-code)  
└ [Adding to Scara](#adding-to-scara)

<a name="introduction"></a>
### Introduction
Scara as built in support for plugins, which allow you to either extend Scara or build libraries to use with Scara. A plugin for Scara has four parts to it, only one being required:

1. The Source Code - REQUIRED (of course..)
2. The Service Provider
3. The Facade (Alias) - Service provider required if you create a facade
4. Configuration

<a name="service-providers"></a>
### Service Providers
Service providers are scripts which add a plugin to Scara's global scope. Service providers are for registering a plugin with Scara, which is used when registering a facade.

To create a new service provider, you need to extend Scara's `ServiceProvider` class, and register it

```php
...
use Scara\Support\ServiceProvider;
use MyVendor\MyPlugin;

class MyPluginServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->create('myplugin', function()
        {
            return new MyPlugin;
        });
    }
}
...
```

The `register()` method is **required** by the `ServiceProvider` class, and is used to register the service provider for your plugin. To use it, you need to include the provider in `config/app.php`

```php
...
'services' => [
    ...
    // Place any custom service providers below
    'MyVendor\MyPlugin\MyPluginServiceProvider',
],
...
```

<a name="facades"></a>
### Facades
Service providers are to be used in conjunction with facades. A facade is simply a class alias, which allows the static calling of class methods. A service provider does nothing alone, and is used to register a plugin. When registering a facade, it calls upon the service provider to get it's information on the plugin. 

To create a new facade, you need to extend Scara's `Facade` class, and register it

```php
...
use Scara\Support\Facades\Facade;

class MyPluginFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'myplugin';
    }
}
...
```

The `getFacadeAccessor()` method is **required** by the `Facade` class, and is used to call upon the service provider. When registering a facade, you return the name registered to Scara by the service provider, so the name you return needs to match the name used to register the plugin from the service provider

In order to use the facade in the application's code, you need to add it to `config/app.php`. Facades are named aliases, so give it a name you can use

```php
...
'aliases' => [
    ...
    // Place any custom aliases below
    'Wazzup' => 'MyVendor\MyPlugin\MyPluginFacade',
],
...
```

Now, you have the ability to call upon your plugin two different ways, normally

```php
...
use MyVendor\MyPlugin\MyPlugin;
...
public function myFunction()
{
    $mp = new MyPlugin;
    echo $mp->sayHello();
}
...
```

Or by using your facade

```php
...
use Wazzup;
...
public function myFunction()
{
    echo Wazzup::sayHello();
}
...
```

<a name="configuration"></a>
### Configuration
If you need to add some extra configuration to your plugin, you can do so by creating a config.php file in the root directory of your plugin. A config script returns an array, which holds your configuration. A config script will look something like this

```php
<?php

return array(
    'name' => 'World',
);
```

You won't be able to access the configuration from the plugin's directory, since Scara doesn't search Composer's vendor directory. Instead, you'll have to publish your configuration to the application. Scara comes with a command built in to do just that

```bash
php cli config:publish myvendor/myplugin
```

The `config:publish` command has one required argument, the plugin name. The plugin name follows the vendor/package scheme. From there, it'll grab the config script, and create a new one in the config directory, which will look like this

```html
$ tree
config
└───pazuzu156
        kparser.php
```

When published, just get the new config value

```php
...
$name = config_get('name', 'mypackage');
echo Wazzup::sayHello($name);

// Hello, World!
```

<a name="plugins-in-action"></a>
### Plugins in Action
To explain plugins for Scara, an example of one is best. Check out <a href="" target="_blank">Pazuzu156/KParser</a> to see how the library is constructed.

<a name="the-source-code"></a>
#### The Source Code
The structure to this library is as follows

```shell
$ tree
kparser
│   composer.json
│   config.php
│   phpunit.xml
│   php_loc.txt
│   README.md
│   run-server.bat
├───src
│   └───Pazuzu156
│       └───KParser
│           │   CodeDocument.php
│           │   KParser.php
│           ├───Laravel
│           │       KParserFacade.php
│           │       KParserServiceProvider.php
│           └───Scara
│                   KParserFacade.php
│                   KParserServiceProvider.php
└───tests
    │   ParsingTest.php
    └───views
            exp1.php
            exp2.php
            exp3.php
            index.php
```

The source code for KParser is under `src/Pazuzu156/KParser` with `config.php` being in the project's root directory. Within the source code directory, a seperate `Scara` directory is there to house the facade and service provider code for Scara. You don't need to add this folder or even put the facade and service provider in there, but for this project, it was to seperate them from Laravel's code.

The structure for the facade and service provider are

```php
<?php

namespace Pazuzu156\KParser\Scara;

use Pazuzu156\KParser\KParser;
use Scara\Config\Configuration;
use Scara\Support\ServiceProvider;

class KParserServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->create('kparser', function () {
            $cc = new Configuration();
            $render = $cc->from('kparser')->get('render_code');

            return new KParser($render);
        });
    }
}
```

```php
<?php

namespace Pazuzu156\KParser\Scara;

use Pazuzu156\KParser\KParser;
use Scara\Support\Facades\Facade;

class KParserFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'kparser';
    }
}
```

<a name="adding-to-scara"></a>
#### Adding to Scara
To register the new plugin, we need to add it to composer, and install it. Then, we can add the service provider and facade to Scara's application configuration

```shell
$ composer require pazuzu156/kparser 1.*
```

Once installed, we need to register it

```php
...
'services' => [
    ...
    'Pazuzu156\KParser\Scara\KParserServiceProvider',
    ...
],
...
'aliases' => [
    ...
    'KParser' => 'Pazuzu156\KParser\Scara\KParserFacade',
    ...
],
...
```

Now that the plugin is registered to Scara, we need to publish KParser's configuration, since it has a configuration file

```shell
$ php cli config:publish pazuzu156/kparser
```

Now, to edit KParser's configuration, just edit `config/pazuzu156/kparser.php`
