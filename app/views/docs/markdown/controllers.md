## Controllers
[Introduction](#introduction)  
[Creating Controllers](#creating-controllers)  
[Rendering a View](#rendering-a-view)  
[Passing Data to a View](#passing-data-to-a-view)  
[Handling Requests](#handling-requests)  
[Redirection](#redirection)  
[Flash Messages](#flash-messages)

<a name="introduction"></a>
### Introduction
Controllers are the first entry into the final page. When you load in a URL, Scara will take it and get the corresponding route from the [Router](/scara/docs/d/routing). Once there's a controller and method to load, it'll be loaded. In turn, the controller will handle any data gathering for models, and render the final view that the user will see.

<a name="creating-controllers"></a>
### Creating Controllers
Creating controllers is easy. A controller skeleton will look something like this

```php
<?php

namespace App\Controllers;

use Scara\Http\Controller;
use Scara\Http\Request;

class HomeController extends Controller
{
	// Place methods here
}
```

Alternatively, you can create a new controller using Scara's command line tool

```shell
$ php cli controller:create HomeController
```

This command will generate the above code in `app/controllers/HomeController.php`

You can also delete a controller by running

```shell
$ php cli controller:drop HomeController
```

<a name="rendering-a-view"></a>
### Rendering a View
Views are handled by <a href="https://laravel.com/docs/5.3/blade" target="_blank">Laravel's Blade Templating Engine</a>. If you have a blade view in `app/views/home.blade.php` you can render this view by using the `render()` method

```php
...
public function getIndex()
{
	$this->render('home');
}
...
```

Rendering views inside sub-folders can be done using dot notation, or using a slash to seperate the sub-folder from the view

```php
...
// render view with dots
$this->render('home.index');

// render view with slashes
$this->render('home/index');
...
```

<a name="passing-data-to-a-view"></a>
### Passing Data to a View
You can pass data along into a view using the `with()` method. Keep in mind, this method needs to be called **BEFORE** calling the `render()` method. Otherwise, the view is rendered before any data is passed to it. Otherwise, use the `renderWithData()` method, which will do both

```php
...
// render chaning with
$this->with('name', 'Kaleb')->render('home.index');

// renderWithData
$this->renderWithData('home.index', ['name' => 'Kaleb']);
...
```

`with()` can also take just an array if you want to pass over an array of data, and not chain a bunch of with statements

```php
...
$data = [
	'name' => 'Kaleb',
	'age' => 23,
];

// render chaning with
$this->with($data)->render('home.index');

// renderWithData
$this->renderWithData('home.index', $data);
...
```

<a name="handling-requests"></a>
### Handing Requests
The router supports both GET and POST requests, and using the `Request` class, you have the ability to interact with request variables passed in from either the route, or by form input

To handle a GET request, either the URL will be formatted using variables in the route, or by standard `?` and `&` from a URL. You'll also need to use the `Request` class as a parameter for your controller's method

```php
...
public function getUser(Request $request)
{
	$user = User::init()->where('username', '=', $request->username)->first();

	$this->with('user', $user)->render('users.getuserinfo');
}
...
```

Post requests are the same as well, only you get access to the `Input` class, which lets you get user input for form validation

<a name="redirection"></a>
### Redirection
Redirection is as simple as calling the `redirect()` method. The redirect method will redirect the user to a given URL. The URL can either be a relative one (relative to Scara) or absolute (to an outside URL)

```php
...
// redirect to another page
$this->redirect('/');

// redirect to another web site
$this->redirect('https://google.com');
...
```

Redirection cannot handle data, however, you can set flash messages while performing a redirect

<a name="flash-messages"></a>
### Flash Messages
Flash messages are messages saved into the session, which are a one-time deal. Setting a message will hold it in the session for the life of the session, unless called upon. Once a flash message is called, it'll be removed from the session.

```php
...
// setting a flash message
$this->flash('flash_message', 'This is a flash message')->render('/');

// calling flash message in view
{{ Session::flash('flash_message') }}
...
```

As stated before, redirects cannot handle data, but a flash message isn't data being passed, it's stored in the session. Thus, you can set one while performing a redirect

```php
// setting flash with redirect
$this->flash('flash_message', 'You have successfully logged in!')->redirect('/users');
```
